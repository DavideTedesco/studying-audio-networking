# Studying Audio Networking

Repositories of resources to study audio networking such us topics related but not limited to:
- Ravenna
- Dante
- AES67
- ISO/OSI pile


## Links

- [Your Practical Guide To AES67- Part 1](https://www.thebroadcastbridge.com/content/entry/9360/your-practical-guide-to-aes67-part-1)
- [Your Practical Guide To AES67- Part 2](https://www.thebroadcastbridge.com/content/entry/9591/your-practical-guide-to-aes67part-2)
- [Your Practical Guide To AES67- Part 3](https://www.thebroadcastbridge.com/content/entry/9653/your-practical-guide-to-aes67part-3)

## Video

- [Audio Sonica, Audio Networking](https://www.audiosonica.com/it/articoli/audio-networking/i-protocolli-di-audio-networking-workshop-audio-networking-video-4-di-5)
